#pragma once
#include <string.h>
#include <termios.h>
#include <termios.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <iostream>
#include <list>
#include <array>

class DisplayModule {
    private:
    std::list<uint8_t> displayBuffer;
    
    std::string portName;
    int portHandle;

    public:
    DisplayModule (std::string path) {
        portName = path;    
    }

    void initialize() {
        struct termios options;
        // Start by connecting at 9600 and telling the module to switch to 115200.  Worst case, it's ignored if we're there already.
        int initPortHandle = open(portName.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
        tcgetattr(initPortHandle, &options);
        cfsetispeed(&options, B9600);
        cfsetospeed(&options, B9600);

        options.c_cflag &= ~CRTSCTS;
        options.c_cflag |= CREAD | CLOCAL;
        options.c_cflag &= ~PARENB;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS8;    
        options.c_oflag &= ~(OPOST | OLCUC);
        tcsetattr(initPortHandle, TCSANOW, &options);

        write(initPortHandle, "SB115200\0", 9);
        close(initPortHandle);
        tcdrain(initPortHandle);

        // Second connection is similar but at 115200.
        sleep(1); // Seems to be required to let the baud change settle
        portHandle = open(portName.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
        options.c_cflag |= CBAUDEX; // seems to be required for 115200.  For 38400 it was not necessary.
        cfsetispeed(&options, B115200);
        cfsetospeed(&options, B115200);
        tcsetattr(portHandle, TCSANOW, &options);
        portHandle = initPortHandle;
        
    
        clearScreen();
        turnOnScreen();
        std::list<uint8_t> image = {
            0b00011000,0b00011000,
            0b00111100,0b00111100,
            0b01111110,0b01111110,
            0b01111111,0b11111110,
            0b01111111,0b11111110,
            0b01111111,0b11111110,
            0b01111111,0b11111110,
            0b01111111,0b11111110,
            0b01111111,0b11111110,
            0b00111111,0b11111100,
            0b00111111,0b11111100,
            0b00001111,0b11110000,
            0b00001111,0b11110000,
            0b00000011,0b11000000,
            0b00000011,0b11000000,
            0b00000001,0b10000000,
        };
        drawImage(48, 0, 16, 16, image);
        setPosition(0, 2);
        writeText("Opening box of  adepti tricks...");
        dumpBuffer();
    }

    void addToBuffer(std::string body)
    {
        const char * ascii = body.c_str();
        int i = 0;
        for (i = 0; i < body.length(); i++) {
            displayBuffer.push_back((uint8_t)ascii[i]);
        }
    }

    void addCharToBuffer(char character)
    {
        displayBuffer.push_back((uint8_t)character);
    }

    void addRawToBuffer(std::list<uint8_t> ascii)
    {
        int i = 0;
        int size = ascii.size();
        for (i = 0; i < size; i++) {
            displayBuffer.push_back(ascii.front());
            ascii.pop_front();
        }
    }

    void turnOffScreen()
    {
        addToBuffer("SOO0");
        dumpBuffer();
    }

    void turnOnScreen()
    {
        addToBuffer("SOO1");
    }
    void clearScreen()
    {
        addToBuffer("CL");
    }

    void writeText(std::string unicodeString)
    {
        addToBuffer("TT" + unicodeString);
        addCharToBuffer(0); 
    }

    void setFont(uint8_t font)
    {
        std::list<uint8_t> command = {(uint8_t)'S', (uint8_t)'F', font};
        addRawToBuffer(command);
    }

    void setPosition(uint8_t x, uint8_t y)
    {
        std::list<uint8_t> command = {(uint8_t)'T', (uint8_t)'P', x, y};
        addRawToBuffer(command);
    }

    void setPrecisePosition(uint8_t x, uint8_t y)
    {
        std::list<uint8_t> command = {(uint8_t)'E', (uint8_t)'T', (uint8_t)'P', x, y};
        addRawToBuffer(command);
    }

    void dumpBuffer()
    {   
        int bufferSize = displayBuffer.size();
        uint8_t bufferArray[bufferSize];
        
        for (int i=0; i<bufferSize;i++) {
            bufferArray[i] = displayBuffer.front();
            //printf("%c(%u) ", bufferArray[i], bufferArray[i]);
            displayBuffer.pop_front();
        }
        //printf("\n\nBuffer: %d\n\n", bufferSize);
        write(portHandle, bufferArray, bufferSize);
        tcdrain(portHandle);
        displayBuffer.clear(); // Likely futile
    }

    void drawLine(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2)
    {
        std::list<uint8_t> command = {(uint8_t)'L', (uint8_t)'N', x1, y1, x2, y2};
        addRawToBuffer(command);
    }

    void drawFilledRectangle(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2)
    {
        std::list<uint8_t> command = {(uint8_t)'F', (uint8_t)'R', x1, y1, x2, y2};
        addRawToBuffer(command);
    }

    void drawImage(uint8_t x, uint8_t y, uint8_t w, uint8_t h, std::list<uint8_t> data)
    {
        std::list<uint8_t> command = {(uint8_t)'D', (uint8_t)'I', (uint8_t)'M', x, y, w, h};
        addRawToBuffer(command);
        addRawToBuffer(data);
    }
};