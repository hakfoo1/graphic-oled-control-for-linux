#include <iostream>
#include <string>
#include <stdio.h>
#include <termios.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <vector>
#include "DisplayModule.hpp"
#include "DisplayScreen.hpp"
#include "MHzScreen.hpp"
#include "DateTimeScreen.hpp"
#include "UsageScreen.hpp"
#include "NetworkScreen.hpp"
#include "DiscScreen.hpp"
#include "LoadScreen.hpp"
#include "SensorScreen.hpp"

std::map<std::string, std::string> prettyNames;

int main(int argc, char** argv) {
    int currentFrame = 0;
    int currentScreen = 0;
    bool advanceScreen = true;
    int i;
    glob_t matches;
    std::string scratchPath;
    std::vector<DisplayScreen *> screens;
    std::string path;
    std::ifstream f;
    std::string line;
    std::stringstream lineStream;
    std::string key, value;

    screens.push_back(new NetworkScreen());
    if (glob("/sys/class/hwmon/*", 0, NULL, &matches) == 0) {
        for (i = 0; i < matches.gl_pathc; i++) {
            scratchPath = std::string(matches.gl_pathv[i] + 17); // (Strip off the "/sys/class/hwmon/")
            screens.push_back(new SensorScreen(scratchPath));
        }
    }    
    screens.push_back(new DiscScreen());
    screens.push_back(new LoadScreen());
    
    screens.push_back(new UsageScreen());
    screens.push_back(new DateTimeScreen());
    screens.push_back(new MHzScreen());


    f.open("/etc/oledrc");
    while(f.good()) {
        std::getline(f, line);
        if (line.length() == 0) {
            break;
        }
    
        lineStream = std::stringstream(line);
        std::getline(lineStream, key, '=');
        std::getline(lineStream, value, '=');

        prettyNames.insert(std::pair<std::string, std::string>(key, value));
    }

    

    
    if (argc > 1) {
        path = std::string(argv[1]);
    } else {
        path = std::string("/dev/ttyUSB0");
    }
    DisplayModule module = DisplayModule(path);

    module.initialize();
    sleep(1);
    while (1) {

        if (currentFrame >= (*screens[currentScreen]).getLifespan()) {
            if (advanceScreen) {
                currentScreen++;
            }
            currentFrame = 0;
        }
        if (currentScreen >= screens.size()) {
            currentScreen = 0;
        }
        
        (*screens[currentScreen]).RenderScreen(module, currentFrame);
        
        currentFrame++;
        usleep(900000);
    }
}