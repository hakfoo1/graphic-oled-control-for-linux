#include "DisplayScreen.hpp"
#include "PrettyNumbers.hpp"
#include <string>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <iostream>
#include <glob.h>
#include <map>

struct sensorData {
    char name[100];
    ulong value;
};

extern std::map<std::string, std::string> prettyNames;

class SensorScreen : public DisplayScreen {
    private:
    std::string path;
    std::string deviceName = "";
    
    void addSensorValues(std::string prettyPrefix, std::string prefix, std::map<ulong, struct sensorData *> * target, bool valueSort = true) {
    
        int i = 0;
        ulong targetIndex;
        ulong value;
        struct sensorData * newItem;
        std::string scratchPath, scratchPath2;
        std::ifstream f, f2;
        std::string line;
        std::stringstream lineStream;
        std::string token;
        glob_t matches;
    
        if (glob(("/sys/class/hwmon/" + std::string(path) + "/" + prefix + "*_input").c_str(), 0, NULL, &matches) == 0) {
            for (i = 0; i < matches.gl_pathc; i++) {
                scratchPath = std::string(matches.gl_pathv[i]);
                scratchPath2 = std::string(matches.gl_pathv[i]);
                scratchPath.replace(scratchPath.length()-5, 5, "label");
                
                f.open(scratchPath2);
                newItem = (struct sensorData *)malloc(sizeof(struct sensorData));
                std::getline(f, line);
                value = stol(line);
                f.close();
                
                f2.open(scratchPath);
            
                if (f2.good()) {
                    std::getline(f2, line);
                    strcpy(newItem->name, std::string(line).c_str());
                } else {
                    strcpy(newItem->name, std::string(prettyPrefix + " " + std::to_string(i + 1)).c_str());
                }
                f2.close();

                
                newItem->name[99] = '\0';
                newItem->value = value;

                
                if (valueSort) {
                    targetIndex = value;
                    while (target->find(targetIndex) != target->end()) {
                        targetIndex++;
                    }
                } else {
                    targetIndex = i;
                }
                target->insert(std::pair<ulong, struct sensorData *>(targetIndex, newItem));
            }
            globfree(&matches);
        }
    }

    std::string prettyName(std::string sensorName)
    {
        if (sensorName == "DEVICENAME") {
            if (prettyNames.find(deviceName) != prettyNames.end()) {
                return prettyNames[deviceName];
            } else {
                return deviceName;
            }
        }

        if (prettyNames.find(deviceName + "." + sensorName) != prettyNames.end()) {
            return prettyNames[deviceName + "." + sensorName];
        }
        return std::string(sensorName);
    }

    public:
    SensorScreen(std::string sensorPath) {
        path = sensorPath;
        std::ifstream f;
        

        f.open("/sys/class/hwmon/" + path + "/device/model");
        if (f.good()) {
            std::getline(f, deviceName);
        }

        f.close();
        if (deviceName == "") {
            f.open("/sys/class/hwmon/" + path + "/name");
            if (f.good()) {
                std::getline(f, deviceName);
            }
            f.close();
        }

        if (deviceName == "") {
            deviceName = path + "?";
        }
        deviceName = deviceName.substr(0, deviceName.find_last_not_of(" \n\r\t") + 1);
    }

    void RenderScreen(DisplayModule display, int currentFrame)
    {
        std::map<ulong, struct sensorData *> temps;
        std::map<ulong, struct sensorData *> fans;
        std::map<ulong, struct sensorData *> clocks;
        std::map<ulong, struct sensorData *>::reverse_iterator looper;
        std::map<ulong, struct sensorData *>::iterator clockLooper;
        char scratchBody[100];
        uint8_t cursorOffset = 0, savedCursorOffset = 0;


        addSensorValues("Fan", "fan", &fans);
        addSensorValues("Temp", "temp", &temps);
        addSensorValues("Clock", "freq", &clocks, false);
        

        display.clearScreen();
        if (clocks.size() > 0) {
            display.setFont(0);
            clockLooper = clocks.begin();
            while (clockLooper != clocks.end()) {
                cursorOffset += 12;
                display.setPrecisePosition(0, cursorOffset);
                sprintf(scratchBody, "%7s %4dMHz", (prettyName(clockLooper->second->name)).substr(0, 7).c_str(), clockLooper->second->value/1000000);
                display.writeText(scratchBody);
                
                clockLooper++;
            }
        } else {
            cursorOffset += 18;
            display.setFont(18);
            display.setPrecisePosition(PrettyNumbers::centeredOffset(prettyName("DEVICENAME").substr(0, 14).c_str(), 9), cursorOffset);
            display.writeText(prettyName("DEVICENAME").substr(0, 14).c_str());
            
        }
        

        display.setFont(10);
        savedCursorOffset = cursorOffset;
        looper = fans.rbegin();
        while (looper != fans.rend()) {
            cursorOffset += 10;
            if (cursorOffset >=63) {
                break;
            }
            display.setPrecisePosition(0, cursorOffset);
            sprintf(scratchBody, "%-5s %4d", (prettyName(looper->second->name)).substr(0, 5).c_str(), looper->second->value);
            display.writeText(scratchBody);
            looper++;
        }
        cursorOffset = savedCursorOffset;

        looper = temps.rbegin();
        while (looper != temps.rend()) {
            cursorOffset += 10;
            if (cursorOffset >=63) {
                break;
            }
            display.setPrecisePosition(64, cursorOffset);
            sprintf(scratchBody, "%-5s %3dc", (prettyName(looper->second->name)).substr(0, 5).c_str(), looper->second->value/1000);
            display.writeText(scratchBody);
            looper++;
        }

        display.setFont(0);
        display.dumpBuffer();

                
        looper = fans.rbegin();
        while (looper != fans.rend()) {
            free(looper->second);
            looper++;
        }

        looper = temps.rbegin();
        while (looper != temps.rend()) {
            free(looper->second);
            looper++;
        }

        clockLooper = clocks.begin();
        while (clockLooper != clocks.end()) {
            free(clockLooper->second);
            clockLooper++;
        }

    }

    std::string getName()
    {
        return "Sensors (" + path + ")";
    }
};