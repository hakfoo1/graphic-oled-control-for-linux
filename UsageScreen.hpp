#include "DisplayScreen.hpp"
#include <vector>

struct cpuStats {
    long user;
    long nice;
    long system;
    long idle;
    long iowait;
    long irq;
    long softirq;
    long steal;
    long guest;
    long guest_nice;
};

class UsageScreen : public DisplayScreen {
    private:
    std::vector<struct cpuStats *> oldPerThread;
    struct cpuStats oldTotal;
    uint8_t barWidth = 1;;
    int coreCount = 0;
    public:
    UsageScreen()
    {
            
        std::ifstream f;
        std::string line;
        std::stringstream lineStream;
        std::string token;
        int cpuNumber = 0;

        f.open("/proc/stat");
        while(f.good()) {
            std::getline(f, line);
            lineStream = std::stringstream(line);
            std::getline(lineStream, token, ' ');
            if (token.substr(0, 3) == "cpu") {
                if (token.length() == 3) {
                    std::getline(lineStream, token, ' ');
                    if (token.length() == 0) {
                        std::getline(lineStream, token, ' '); // Not sure if getline gets the second space as a new field.
                    }
                    oldTotal.user = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldTotal.nice = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldTotal.system = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldTotal.idle = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldTotal.iowait = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldTotal.irq = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldTotal.softirq = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldTotal.steal = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldTotal.guest = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldTotal.guest_nice = stol(token);

                } else {
                    cpuNumber = stoi(token.substr(3));
                    
                    oldPerThread.push_back((struct cpuStats *)malloc(sizeof(struct cpuStats)));

                    std::getline(lineStream, token, ' ');
                    oldPerThread[cpuNumber]->user = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldPerThread[cpuNumber]->nice = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldPerThread[cpuNumber]->system = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldPerThread[cpuNumber]->idle = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldPerThread[cpuNumber]->iowait = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldPerThread[cpuNumber]->irq = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldPerThread[cpuNumber]->softirq = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldPerThread[cpuNumber]->steal = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldPerThread[cpuNumber]->guest = stol(token);
                    std::getline(lineStream, token, ' ');
                    oldPerThread[cpuNumber]->guest_nice = stol(token);
                    coreCount++;
                }
            }
        }
        f.close();
        barWidth = (uint8_t)((128 / coreCount) -2);
    }

    
    void RenderScreen(DisplayModule display, int currentFrame)
    {
        std::ifstream f;
        std::string line;
        std::stringstream lineStream;
        std::string token;
        struct cpuStats current;
        std::vector <double> coreUsage;
        int cpuNumber = 0;
        double total = 0;
        double nonIdle = 0;
        double mainCpu = 0;
        long totalMemory = 0;
        long freeMemory = 0;
        long totalSwap = 0;
        long freeSwap = 0;
        int i;
        char stagingText[64];

        f.open("/proc/stat");
        while(f.good()) {
            std::getline(f, line);
            lineStream = std::stringstream(line);
            std::getline(lineStream, token, ' ');
            if (token.substr(0, 3) == "cpu") {
            
                if (token.length() == 3) {
                    std::getline(lineStream, token, ' ');
                    if (token.length() == 0) {
                        std::getline(lineStream, token, ' '); // Not sure if getline gets the second space as a new field.
                    }
                    current.user = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.nice = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.system = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.idle = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.iowait = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.irq = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.softirq = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.steal = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.guest = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.guest_nice = stol(token);

                    total = (current.user - oldTotal.user) +
                              (current.nice - oldTotal.nice) +
                              (current.system - oldTotal.system) +
                              (current.idle - oldTotal.idle) +
                              (current.iowait - oldTotal.iowait) +
                              (current.irq - oldTotal.irq) +
                              (current.softirq - oldTotal.softirq) +
                              (current.steal - oldTotal.steal) +
                              (current.guest - oldTotal.guest) +
                              (current.guest_nice - oldTotal.guest_nice);
                    
                    nonIdle = (current.user - oldTotal.user) +
                              (current.nice - oldTotal.nice) +
                              (current.system - oldTotal.system) +
                              (current.iowait - oldTotal.iowait) +
                              (current.irq - oldTotal.irq) +
                              (current.softirq - oldTotal.softirq) +
                              (current.steal - oldTotal.steal) +
                              (current.guest - oldTotal.guest) +
                              (current.guest_nice - oldTotal.guest_nice);
                    mainCpu = 100 * (nonIdle/total);

                    oldTotal = current;
                } else {
                    cpuNumber = stoi(token.substr(3));

                    std::getline(lineStream, token, ' ');
                    current.user = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.nice = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.system = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.idle = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.iowait = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.irq = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.softirq = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.steal = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.guest = stol(token);
                    std::getline(lineStream, token, ' ');
                    current.guest_nice = stol(token);


                    total = (current.user - oldPerThread[cpuNumber]->user) +
                              (current.nice - oldPerThread[cpuNumber]->nice) +
                              (current.system - oldPerThread[cpuNumber]->system) +
                              (current.idle - oldPerThread[cpuNumber]->idle) +
                              (current.iowait - oldPerThread[cpuNumber]->iowait) +
                              (current.irq - oldPerThread[cpuNumber]->irq) +
                              (current.softirq - oldPerThread[cpuNumber]->softirq) +
                              (current.steal - oldPerThread[cpuNumber]->steal) +
                              (current.guest - oldPerThread[cpuNumber]->guest) +
                              (current.guest_nice - oldPerThread[cpuNumber]->guest_nice);
                    
                    nonIdle = (current.user - oldPerThread[cpuNumber]->user) +
                              (current.nice - oldPerThread[cpuNumber]->nice) +
                              (current.system - oldPerThread[cpuNumber]->system) +
                              (current.iowait - oldPerThread[cpuNumber]->iowait) +
                              (current.irq - oldPerThread[cpuNumber]->irq) +
                              (current.softirq - oldPerThread[cpuNumber]->softirq) +
                              (current.steal - oldPerThread[cpuNumber]->steal) +
                              (current.guest - oldPerThread[cpuNumber]->guest) +
                              (current.guest_nice - oldPerThread[cpuNumber]->guest_nice);
                    coreUsage.push_back(100 * nonIdle/total);
                    
                    * oldPerThread[cpuNumber] = current;
                }
            }
        }
        f.close();


        f.open("/proc/meminfo");
        while(f.good()) {
            std::getline(f, line);
            lineStream = std::stringstream(line);
            std::getline(lineStream, token, ' ');
            if (token == "MemTotal:") {
                std::getline(lineStream, token, ' ');
                while (token == "") {
                    std::getline(lineStream, token, ' ');
                }
                totalMemory = stol(token);
            }
            if (token == "MemFree:") {
                std::getline(lineStream, token, ' ');
                while (token == "") {
                    std::getline(lineStream, token, ' ');
                }
                freeMemory = stol(token);
            }
            if (token == "SwapTotal:") {
                std::getline(lineStream, token, ' ');
                while (token == "") {
                    std::getline(lineStream, token, ' ');
                }
                totalSwap = stol(token);
            }
            if (token == "SwapFree:") {
                std::getline(lineStream, token, ' ');
                while (token == "") {
                    std::getline(lineStream, token, ' ');
                }
                freeSwap = stol(token);
            }
        }
        f.close();
    
        //ulong memoryUsage = memory - (ulong)memoryCounter.NextValue();

        
        display.clearScreen();
            
        display.setFont(18);
        display.setPrecisePosition(0, 25);
        sprintf(stagingText, "%5.1f%%", mainCpu);
        display.writeText(std::string(stagingText));
        
        display.setPrecisePosition(57, 10);
        display.writeText(PrettyNumbers::formatNumber((totalMemory - freeMemory) * 1024, 7));

        display.setPrecisePosition(57, 25);
        display.writeText(PrettyNumbers::formatNumber((totalSwap - freeSwap) * 1024, 7));
        display.setFont(0);


        display.drawLine(56, 0, 56, 32);
        display.drawLine(0, 32, 127, 32);
        display.drawLine(0, 63, 127, 63);
        uint8_t basex = (uint8_t)(64 - (coreCount * (barWidth + 2)) / 2);
        float percentage = 0;
        for (i = 0; i < coreCount; i++) {
            basex++;
            percentage = coreUsage[i];
            display.drawFilledRectangle(basex, (uint8_t) (62-(30 * percentage / 100)), (uint8_t)(basex + barWidth), 62);
            basex += barWidth;
            basex++;
           
        }
        display.dumpBuffer();
    }

    std::string getName()
    {
        return "CPU/RAM";
    }   
};