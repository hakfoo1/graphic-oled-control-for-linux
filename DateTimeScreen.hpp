#include "DisplayScreen.hpp"
#include "PrettyNumbers.hpp"
#include <stdio.h>
#include <string>
#include <fstream>
#include <sstream>
#include <math.h>
#include <time.h>

class DateTimeScreen : public DisplayScreen {
    public:
    void RenderScreen(DisplayModule display, int currentFrame)
    {

        std::ifstream f;
        std::string line;
        std::stringstream lineStream;
        std::string token;
        char formattedString[32];
        std::string formatString;
        char currentTime[30];
        char currentDate[30];
        std::string dayNames[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
        float uptime = 0;
        struct tm * current;
        time_t timet;

        timet = time(0);
        current = localtime(&timet);

        sprintf(currentTime, "%02d:%02d:%02d", current->tm_hour, current->tm_min, current->tm_sec);
        sprintf(currentDate, "%3s. %04d-%02d-%02d", dayNames[current->tm_wday].c_str(), current->tm_year + 1900, current->tm_mon + 1, current->tm_mday);

        
        f.open("/proc/uptime");
        std::getline(f, line);
    
        lineStream = std::stringstream(line);
        std::getline(lineStream, token, ' ');

        uptime=atof(token.c_str());
        f.close();
        int days = (int)floor(uptime / 86400);
        uptime -= days * 86400;
        int hours = (int)floor(uptime / 3600);
        uptime -= hours * 3600;
        int mins = (int)floor(uptime / 60);
        uptime -= mins * 60;
        
        display.clearScreen();
        display.setFont(120);
        display.setPosition(2, 0);
        display.writeText(currentTime);
        display.setFont(0);

        display.setPrecisePosition(4, 45);
        display.writeText(currentDate);
        display.setPrecisePosition(0, 60);
    
        
        if (days != 1) {
            formatString = "%d days %02d:%02d.%02d";
        } else {
            formatString = "%d day %02d:%02d.%02d";
        }
        
        sprintf(formattedString, formatString.c_str(), days, hours, mins, (int)uptime);
        display.setPrecisePosition(PrettyNumbers::centeredOffset(formattedString), 60);
        
    
        display.writeText(std::string(formattedString));
        display.dumpBuffer();
    }

    std::string getName()
    {
        return "Time and Uptime";
    }
};