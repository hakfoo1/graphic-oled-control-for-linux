#include "DisplayScreen.hpp"
#include <stdio.h>
#include <string>
#include <fstream>
#include <sstream>
#include <math.h>

class MHzScreen : public DisplayScreen {
    public:
    void RenderScreen(DisplayModule display, int currentFrame)
    {
    
        std::ifstream f;
        std::string line;
        std::stringstream lineStream;
        std::string token;
        int first;
        int last;
        float mhz = 0;
        float min = 99999999;
        float max = 0;
        float sum = 0;
        float avg = 0;
        char displayLine[32];
        int cores = 0;
    
        f.open("/proc/cpuinfo");
        while(f.good()) {
            std::getline(f, line);
            lineStream = std::stringstream(line);
            std::getline(lineStream, token, ':');
            if (token.substr(0, 7) == "cpu MHz") {
                std::getline(lineStream, token, ':');        
                first = token.find_first_not_of(" \r\n");
                last = token.find_last_not_of(" \r\n");
                mhz = atof(token.substr(first, last-first-1).c_str());
                if (mhz < min) {
                    min = mhz;
                }
                if (mhz > max) {
                    max = mhz;
                }
                sum += mhz;
                cores++;
            }
        }
        f.close();
        avg = sum/(float)cores;
        sprintf(displayLine, "%d", (int)max);
        display.clearScreen();
        display.setFont(123);
        display.setPrecisePosition(6, 35);
        display.writeText(displayLine);
    
        display.setFont(10);
        display.setPrecisePosition(0, 50);
        sprintf(displayLine, "Min: %4d - Avg: %4d", (int)min, (int)avg);
        display.writeText(displayLine);
        display.setFont(0);
        display.dumpBuffer();
    }

    std::string getName()
    {
        return "Processor Clocks";
    }
};