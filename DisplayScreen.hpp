#pragma once
#include <string>
#include "DisplayModule.hpp"

class DisplayScreen
{
    public:
    virtual void RenderScreen(DisplayModule display, int currentFrame)
    {
        display.clearScreen();
        display.writeText("Placeholder screen not yet implemented");
        display.dumpBuffer();
    }

    virtual std::string getName()
    {
        return "Placeholder Screen";
    }

    virtual int getLifespan()
    {
        return 30;
    }
};