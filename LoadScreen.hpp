#include "DisplayScreen.hpp"
#include "PrettyNumbers.hpp"
#include <string>
#include <fstream>
#include <sstream>
#include <unistd.h>

class LoadScreen : public DisplayScreen {
    public:
    void RenderScreen(DisplayModule display, int currentFrame)
    {

        std::ifstream f;
        std::string line;
        std::stringstream lineStream;
        std::string token, l1, l5, l15;
        char hostname[128];
        
        f.open("/proc/loadavg");
        std::getline(f, line);
        gethostname(hostname, 10);
        hostname[10] = '\0'; // in case we truncated and it rolls off, we get a trailing \0
        lineStream = std::stringstream(line);
        std::getline(lineStream, l1, ' ');
        std::getline(lineStream, l5, ' ');
        std::getline(lineStream, l15, ' ');
        display.clearScreen();
        display.setFont(51);
        // Font 51 is variable width for text, so assuming an average 12 pixels/char may be wrong for some names
        display.setPrecisePosition(PrettyNumbers::centeredOffset(hostname, 12), 20);
        display.writeText(hostname);
        display.setFont(18);
        display.setPrecisePosition(2, 35);
        display.writeText(l1);

        display.setPrecisePosition(PrettyNumbers::centeredOffset(l5, 9), 50);
        display.writeText(l5);

        display.setPrecisePosition(128-l15.length() * 9 -2, 63);
        display.writeText(l15);
        display.setFont(0);

        display.dumpBuffer();
    }

    std::string getName()
    {
        return "Load Average";
    }
};