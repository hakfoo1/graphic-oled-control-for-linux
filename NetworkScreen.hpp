#include "DisplayScreen.hpp"
#include <string>
#include <sys/time.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <ifaddrs.h>
#include "PrettyNumbers.hpp"

class NetworkScreen : public DisplayScreen {
    private:
    char ip[50] = "No IPv4";
    char ip6[50] = "No IPv6";
    std::string cardName = "";
    ulong down = 0;
    ulong up = 0;
    double lastChecked = 0;
    std::list<uint8_t> downArrow = {
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b11111111,
        0b01111110,
        0b00111100,
        0b00011000,
    };

    std::list<uint8_t> upArrow = {
        0b00011000,
        0b00111100,
        0b01111110,
        0b11111111,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
        0b00011000,
    };

    public:
    NetworkScreen()
    {
        struct timeval timestamp;
        gettimeofday(&timestamp, NULL);
        lastChecked = timestamp.tv_sec + ((double)timestamp.tv_usec / 1000000);
    }

    void getCard() 
    {
        std::ifstream f;
        std::string line;
        std::stringstream lineStream;
        std::string token;
        std::string name;
        struct ifaddrs * addresses = NULL;
        struct ifaddrs * address = NULL;
        
        ulong currentDown = 0;
        ulong currentUp = 0;
        int i;
        
        f.open("/proc/net/dev");
        while(f.good()) {
            std::getline(f, line);
            if (line.length() == 0) {
                break;
            }
            lineStream = std::stringstream(line);
        
            std::getline(lineStream, token, ' ');
            while (token == "") {
                std::getline(lineStream, token, ' ');
            }
            if (token.back() == ':') {
                name = token.substr(0, token.length()-1);
                if (name == "lo") {
                    continue;
                }
                std::getline(lineStream, token, ' ');
                while (token == "") {
                    std::getline(lineStream, token, ' ');
                }
                currentDown = stol(token);
                for (i = 0; i < 8; i++) {
                std::getline(lineStream, token, ' ');
                    while (token == "") {
                        std::getline(lineStream, token, ' ');
                    }
                }
                currentUp = stol(token);
                if (currentDown > down || currentUp > up) {
                    cardName = name;
                    down = currentDown;
                    up = currentUp;
                }
            }
        }

        getifaddrs(&addresses);
        address = addresses;
        while (address != NULL) {
            if (strcmp(address->ifa_name, cardName.c_str()) == 0) {
                if(address->ifa_addr->sa_family == AF_INET) {
                    inet_ntop(address->ifa_addr->sa_family, &((struct sockaddr_in *)address->ifa_addr)->sin_addr, ip, 50);
                }

                if(address->ifa_addr->sa_family == AF_INET6) {
                    inet_ntop(address->ifa_addr->sa_family, &((struct sockaddr_in6 *)address->ifa_addr)->sin6_addr, ip6, 50);
                }
            }
            address = address->ifa_next;
        }

        f.close();
    }

    
    void RenderScreen(DisplayModule display, int currentFrame)
    {
        struct timeval timestamp;
        std::ifstream f;
        std::string line;
        std::stringstream lineStream;
        std::string token;
        std::string name;
        std::string wireless = "NA";
        char staging[128];
        int i;
        ulong currentDown = 0;
        ulong currentUp = 0;
        double nowChecked;

        if(currentFrame == 0) {
            getCard();
        }

        gettimeofday(&timestamp, NULL);
        nowChecked = timestamp.tv_sec + ((double)timestamp.tv_usec / 1000000);
        double period = (nowChecked - lastChecked);
        f.open("/proc/net/dev");
        while(f.good()) {
            std::getline(f, line);
            if (line.length() == 0) {
                break;
            }
            lineStream = std::stringstream(line);
            std::getline(lineStream, token, ' ');
        
            while (token == "") {
                std::getline(lineStream, token, ' ');
            }
            if (token.back() == ':') {
                name = token.substr(0, token.length()-1);
                if (name != cardName) {
                    continue;
                }
                std::getline(lineStream, token, ' ');
                while (token == "") {
                    std::getline(lineStream, token, ' ');
                }
                currentDown = stol(token);
                for (i = 0; i < 8; i++) {
                std::getline(lineStream, token, ' ');
                    while (token == "") {
                        std::getline(lineStream, token, ' ');
                    }
                }
                currentUp = stol(token);
            }
        }
        f.close();
        
        f.open("/proc/net/wireless");
        while(f.good()) {
            std::getline(f, line);
            if (line.length() == 0) {
                break;
            }
            lineStream = std::stringstream(line);
            std::getline(lineStream, token, ' ');
        
            while (token == "") {
                std::getline(lineStream, token, ' ');
            }
            if (token.back() == ':') {
                name = token.substr(0, token.length()-1);
                if (name != cardName) {
                    continue;
                }
                for (i = 0; i < 3; i++) {
                std::getline(lineStream, token, ' ');
                    while (token == "") {
                        std::getline(lineStream, token, ' ');
                    }
                }
                float db = std::stof(token);
                sprintf(staging, "%-.0f", db);
                wireless = wireless = std::string(staging) + "dB";
            }
        }
        f.close();

        display.clearScreen();
        display.setPrecisePosition(PrettyNumbers::centeredOffset(ip), 12);
        display.writeText(ip);
        
        display.setFont(6);
        if (strlen(ip6) > 32) {
            display.setPrecisePosition(0, 20);
            char firstpart[33];
            strncpy(firstpart, ip6, 32);
            firstpart[32] = '\0';
            
            display.writeText(firstpart);
            display.setPrecisePosition(PrettyNumbers::centeredOffset(ip6+32,4), 26);
            display.writeText(ip6+32);
        } else {
            display.setPrecisePosition(PrettyNumbers::centeredOffset(ip6, 4), 20);
            display.writeText(ip6);
        }
        display.drawImage(0, 34, 8, 12, downArrow);
        
        display.setFont(10);
        display.setPrecisePosition(8, 38);
        display.writeText(PrettyNumbers::formatNumber(currentDown));
        display.setPrecisePosition(8, 48);
        display.writeText(PrettyNumbers::formatNumber((ulong) ((currentDown - down) / period),5) + "/s");
        display.setPrecisePosition(72, 38);
        display.writeText(PrettyNumbers::formatNumber(currentUp));
        display.setPrecisePosition(72, 48);
        display.writeText(PrettyNumbers::formatNumber((ulong) ((currentUp - up) / period),5)+ "/s");
        display.setFont(0);
        display.setPrecisePosition(0, 64);
        display.writeText(cardName);
        display.setPrecisePosition(128 - 8 * wireless.length(), 64);
        display.writeText(wireless);
        display.drawImage(60, 34, 8, 12, upArrow);
        display.dumpBuffer();

        down = currentDown;
        up = currentUp;
        lastChecked = nowChecked;
                
    }

    std::string getName()
    {
        return "Network";
    }   
};