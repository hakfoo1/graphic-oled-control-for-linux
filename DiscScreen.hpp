#include "DisplayScreen.hpp"
#include <string>
#include <sys/statvfs.h>
#include <map>
#include <vector>
#include <list>
#include <iterator>
#include <glob.h>
#include <filesystem>
#include "PrettyNumbers.hpp"

struct fsDetails {
    char path[500];
    ulong total;
    ulong free;
    int type;
};

class DiscScreen : public DisplayScreen {
    private:
    std::vector<std::list<uint8_t>> icons = {
        // Type 0, unknown
         {
          0b00000000,0b00000000,  
          0b00011111,0b11111000,   
          0b01111111,0b11111110,   
          0b11110000,0b00001111,
          0b00000000,0b00001111,
          0b00000000,0b00011110,
          0b00000000,0b00111100,
          0b00000000,0b01111000,
          0b00000000,0b11110000,
          0b00000001,0b11100000,
          0b00000011,0b11000000,
          0b00000011,0b11000000,
          0b00000000,0b00000000,
          0b00000011,0b11000000,
          0b00000011,0b11000000,
          0b00000000,0b00000000,
        },
        // Type 1, No root
        {
          0b00000000,0b00000000,  
          0b11110000,0b00001111,   
          0b01111000,0b00011110,
          0b01111000,0b00011110,
          0b00111100,0b00111100,
          0b00011110,0b01111000,
          0b00011110,0b01111000,
          0b00001111,0b11110000,
          0b00001111,0b11110000,
          0b00011110,0b01111000,
          0b00011110,0b01111000,
          0b00111100,0b00111100,
          0b01111000,0b00011110,
          0b01111000,0b00011110,
          0b11110000,0b00001111,   
          0b00000000,0b00000000,
        },
        // Type 2, Removable
        {
          0b00000000,0b00000000,  
          0b11111111,0b11111111,   
          0b10100000,0b00000101,   
          0b10100000,0b00000101,
          0b10100000,0b00000101,
          0b10011111,0b11111001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b10001111,0b11110001,
          0b10010000,0b00001001,
          0b10010110,0b00001001,
          0b10010110,0b00001001,
          0b10010110,0b00001001,
          0b01010000,0b00001001,
          0b00111111,0b11111111,
          0b00000000,0b00000000,
        },
        // Type 3, Local
        {
          0b00000000,0b00000000,  
          0b00001111,0b11110000,   
          0b11110000,0b00001111,   
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b11110000,0b00001111,
          0b10001111,0b11110001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b10000000,0b00000001,
          0b11110000,0b00001111,
          0b00001111,0b11110000,
          0b00000000,0b00000000,
        },
        // Type 4, Network
        {
          0b00000000,0b00000000,  
          0b11100000,0b00000111,   
          0b11110000,0b00000111,
          0b11111000,0b00000111,
          0b11111100,0b00000111,
          0b11101110,0b00000111,
          0b11100111,0b00000111,
          0b11100011,0b10000111,
          0b11100001,0b11000111,
          0b11100000,0b11100111,
          0b11100000,0b01110111,
          0b11100000,0b00111111,
          0b11100000,0b00011111,
          0b11100000,0b00001111,   
          0b11100000,0b00000111,   
          0b00000000,0b00000000,
        },
        // Type 5, CD
        {
          0b00000000,0b00000000,  
          0b00000011,0b11000000,   
          0b00011100,0b00111000,   
          0b01100000,0b00000110,
          0b11000000,0b00000011,
          0b10000000,0b00000001,
          0b10000111,0b11100001,
          0b10001000,0b00010001,
          0b10001000,0b00010001,
          0b10000111,0b11100001,
          0b10000000,0b00000001,
          0b11000000,0b00000011,
          0b01100000,0b00000110,
          0b00011100,0b00111000,
          0b00000011,0b11000000,
          0b00000000,0b00000000,
        },
        // Type 6, RAM disc
        {
          0b00000000,0b00000000,  
          0b11111111,0b11100000,   
          0b11111111,0b11111000,
          0b11100000,0b00011100,
          0b11100000,0b00011100,
          0b11100000,0b00011100,
          0b11100000,0b01111000,
          0b11111111,0b11100000,
          0b11111111,0b11000000,
          0b11100000,0b11100000,
          0b11100000,0b01110000,
          0b11100000,0b00111000,
          0b11100000,0b00011100,
          0b11100000,0b00001110,   
          0b11100000,0b00000111,   
          0b00000000,0b00000000,
        }
    };

    public:
    DiscScreen()
    {

    }


    
    void RenderScreen(DisplayModule display, int currentFrame)
    {
        if (currentFrame % 15 != 0) {
            return;
        }
        std::map<ulong, struct fsDetails *> filesystems;
        std::ifstream f, f2;
        std::string line;
        std::stringstream lineStream;
        std::string token;
        std::string name;
        std::string path;
        std::string fsType;
        std::error_code errorReturn;
        std::string scratchPartitionName;
        glob_t searches;
        int found = 0;
        std::string removable = "0";
        struct statvfs64 fs;
        struct fsDetails * details;
        char staging[128];
        ulong targetIndex;
        size_t offset;
        f.open("/proc/mounts");
        while(f.good()) {
            std::getline(f, line);
            if (line.length() == 0) {
                break;
            }
        
            lineStream = std::stringstream(line);
            std::getline(lineStream, token, ' ');
        
            if (token.substr(0, 5) == "/dev/") {
                scratchPartitionName = token.substr(5);
                removable = "0";
                found = 0;
                while (scratchPartitionName.length() > 0 && !found) {
                    glob(std::string("/sys/block/"+scratchPartitionName+"/removable").c_str(), 0, NULL, &searches);
                    if (searches.gl_pathc > 0) {
                        found = 1;
                        f2.open("/sys/block/"+scratchPartitionName+"/removable");
                        std::getline(f2, removable);
                        f2.close();
                    } else {
                        scratchPartitionName = scratchPartitionName.substr(0, scratchPartitionName.length() - 1);
                    }
                }

                std::getline(lineStream, token, ' ');
                offset = token.find("\\040");
                while (offset != std::string::npos) {
                    token.replace(offset, 4, " ");
                    offset = token.find("\\040");
                }
                
                if (statvfs64(token.c_str(), &fs) == 0) {
                    
                    details = (struct fsDetails *)malloc(sizeof(struct fsDetails));
                    
                    strncpy(details->path, token.c_str(), 499);
                    details->path[499] = '\0';
                    details->free = (ulong)fs.f_bavail * (ulong)fs.f_bsize;
                    details->total = (ulong)fs.f_blocks * (ulong)fs.f_bsize;
                
                    std::getline(lineStream, fsType, ' ');
                    
                    details->type = 0;
                    if (removable == "1") {
                        details->type = 2;
                    } else {
                        details->type = 3;
                    }
                    // Optical drives get special treatment
                    if (fsType == "iso9660") details->type = 5;
                    if (fsType == "udf") details->type = 5;
                    
                    targetIndex = details->total;
                    while (filesystems.find(targetIndex) != filesystems.end()) {
                        targetIndex++;
                    }
                
                    filesystems.insert(std::pair<ulong, struct fsDetails *>(targetIndex, details));
                }
            }
        }
     
        f.close();
        std::map<ulong, struct fsDetails *>::reverse_iterator looper = filesystems.rbegin();
        int yOffset = 1;
        int driveType = 0;
        display.clearScreen();
        while (looper != filesystems.rend()) {
            if (yOffset > 3) {
                break;
            }
            display.setFont(0);
            display.setPrecisePosition(24, (uint8_t)(21 * yOffset)-10);
            if(strlen(looper->second->path) > 12) {
                display.writeText("..." + std::string(looper->second->path + strlen(looper->second->path) - 10));
            } else {
                display.writeText(std::string(looper->second->path));
            }
            display.setFont(10);
            driveType = looper->second->type;
            if (driveType < icons.size()) {
                display.drawImage(0, (uint8_t) (21 * yOffset - 17), 16, 16, icons[driveType]);
            } else {
                display.drawImage(0, (uint8_t) (21 * yOffset - 17), 16, 16, icons[0]);
            }
            display.setPrecisePosition(24, (uint8_t)(21 * yOffset));
            display.writeText(PrettyNumbers::formatNumber(looper->second->total - looper->second->free, 6) + " / " + PrettyNumbers::formatNumber(looper->second->total, 6));
            display.setFont(0);

            
            yOffset++;
            looper++;
            
        }

        display.dumpBuffer();

        looper = filesystems.rbegin();
        while (looper != filesystems.rend()) {
            free(looper->second);
            looper++;
        }
    }

    std::string getName()
    {
        return "Discs";
    }   
};