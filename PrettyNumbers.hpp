#pragma once
#include <algorithm>
#include <string>
#include <stdio.h>

class PrettyNumbers
{
    public:
    
    static std::string formatNumber(ulong number, int length = 7)
    {
        char placeholder[255];
        // NNNN.NNT on 7
        // Integer values have 1 character less than length to work with
        // Floating point has two less.
        if (number > 10736344498176) { // Over 9999G -- value is NNNN.NNT or
            sprintf(placeholder, std::string("%" + std::to_string(length) + "." + std::to_string(std::max(length - 6, 0)) + "fT").c_str(), (float)number/1099511627776);
            return std::string(placeholder);
        }
        if (number > 1072668082176) { // Over 999G value is NNNN.NNG
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 6, 0)) + "fG").c_str(), (float)number/(1024*1024*1024));
            return std::string(placeholder);
        }
        if (number > 106300440576) { // Over 99G value is NNN.NNNG
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 5, 0)) + "fG").c_str(), (float)number/(1024*1024*1024));
            return std::string(placeholder);
        }
        if (number > 9663676416) { // Over 9G value is NN.NNNNG
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 4, 0)) + "fG").c_str(), (float)number/(1024*1024*1024));
            return std::string(placeholder);
        }
        
        if (number > 10484711424) { // Over 9999M value is N.NNNNNG
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 3, 0)) + "fG").c_str(), (float)number/(1024*1024*1024));
            return std::string(placeholder);
        }

        if (number > 1047527424) { // Over 999M value is NNNN.NNM
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 6, 0)) + "fM").c_str(), (float)number/(1024*1024));
            return std::string(placeholder);
        }
        if (number > 103809024) { // Over 99M value is NNN.NNNM
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 5, 0)) + "fM").c_str(), (float)number/(1024*1024));
            return std::string(placeholder);
        }
        if (number > 9437184) { // Over 9M value is NN.NNNNM
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 4, 0)) + "fM").c_str(), (float)number/(1024*1024));
            return std::string(placeholder);
        }

        if (number > 9999*1024) { // Over 9999k, value is N.NNNNNM
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 3, 0)) + "fM").c_str(), (float)number/(1024*1024));
            return std::string(placeholder);
        }
        if (number > 999*1024) { // Over 999k, value is NNNN.NNK
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 6, 0)) + "fK").c_str(), (float)number/(1024));
            return std::string(placeholder);
        }

        if (number > 99*1024) { // Over 99k, value is NNN.NNNK
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 5, 0)) + "fK").c_str(), (float)number/(1024));
            return std::string(placeholder);
        }

        if (number > 9*1024) { // Over 9k, value is NN.NNNNK
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 4, 0)) + "fK").c_str(), (float)number/(1024));
            return std::string(placeholder);
        }

        if (number > 999) { // Over 999, value is NNNN.NNK
            sprintf(placeholder, std::string("%" + std::to_string(length)+"." + std::to_string(std::max(length - 3, 0)) + "fK").c_str(), (float)number/(1024));
            return std::string(placeholder);
        }
        
        sprintf(placeholder, std::string("%" + std::to_string(length) + "dB").c_str(), number);
        return std::string(placeholder);
    }   
    static uint8_t centeredOffset(char * message, int averageSize = 8) {
        return (uint8_t)(std::max((uint8_t)0, (uint8_t)(64 - ((std::string(message).length() * averageSize) / 2))));
    }

    static uint8_t centeredOffset(std::string message, int averageSize = 8) {
        return (uint8_t)(std::max((uint8_t)0, (uint8_t)(64 - ((message.length() * averageSize) / 2))));
    }
};