# Graphic OLED Control for Linux

Linux utility to consolidate system info and display it on a serial-accessed OLED module

## Getting started
Expected to exist:
* Sensor info in /sys/class/hwmon -- likely from lm_sensors
* USB serial adaptor expected at /dev/ttyUSB0

This is specifically designed around the Digole serial display module DS12864OLED-3W, using the USB-to-UART module they also sell.  Different Digole modules should work, but you may want to tweak layout geometry and designs, as this strongly assumes 128x64, particularly in the PrettyNumbers::centeredOffset method.

## Installation
Compile with
gcc *.cpp -o (desired executable name)
You can override the path /dev/ttyUSB0 by adding the desired port name as a command line parameter.
You'll likely have to run it as root/setuid or adjust permissions to make sure you can write to the desired serial port.
Launching it on boot is an exercise for the reader, as it will vary enormously by which init-system you use.  It's suggested you launch it after networking becomes available, because it only looks for addresses at launch-time.

## Configuration
The program expects a configuration file at /etc/oledrc.  It will contain name-value pairs for sensor name remapping, because a lot of sensors have, to be blunt, non-friendly naming.
These can take two forms:

 * DeviceName.SensorName=Human Readable Name
 * DeviceName=Human Readable Name

The DeviceName is:

 * For devices that have /sys/class/hwmon/(device ID)/device/model, the name given there.  I've seen this for NMVe SSDs, for example
 * For devices that don't have that, the name given in /sys/class/hwmon/(device ID)/name  -- this is often something driver-centric like "amdgrp" or "iwlwifi_1"
 * For devices lacking even that, it will be (device ID)? -- i. e. "hwmon4?" which is intended to mean "find a better way to detect this"

The SensorName is the name in the file temp(ID).label, fan(ID)_label, or freq(id)_label.  If there isn't a label file, an automatic name is generated like "Fan 2" or "Temp 6" or "Clock 1"
Most of the displays are cut to 5 characters long, while clock-speed ones are 7.


An example looks like

```
nct6687=Mainboard
nct6687.Fan 2=CPU
nct6687.Fan 3=Pump
nct6687.Fan 4=Rear
nct6687.Fan 9=Front
nct6687.Thermistor 1=MB 2
SHGP31-1000GM.Composite=SSD
```
